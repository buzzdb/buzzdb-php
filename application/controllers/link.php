<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Link extends CI_Controller {

    public function index() {
        $this->all();
    }

    public function all() {
        $this->load->model('MLink');
        $data['links'] = $this->MLink->get_last_entries();
        $this->load->view('link_all', $data);
    }
	public function add() {
        $this->load->view('form_add_link');
	}
	public function add_commit() {
		$this->load->model('MLink');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('title', 'Title', 'required');		
		$this->form_validation->set_rules('url', 'Url', 'required');
		if ($this->form_validation->run() == FALSE) {
			redirect ('link/add');
		} else {
			$title = $this->input->post('title');
			$url = $this->input->post('url');
			$tags = preg_split ("/, */", $this->input->post ('tags'));
			$this->MLink->add_link($url, $title, $tags);
			redirect ('');
		}
	}

	public function view($id) {
		$this->load->model('MComment');
		$data['comments'] = $this->MComment->from_article($id);
		$this->load->model('MLink');
		$data['link'] = $this->MLink->get_by_id($id);
		$this->load->view('link_view', $data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/link.php */

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comment extends CI_Controller {

    public function index() {
    }

	public function add($parent) {
        $this->load->view('form_add_comment', array ('parent' => $parent));
	}
	public function add_commit() {
		$this->load->model('MComment');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('author', 'Author', 'required');		
		$this->form_validation->set_rules('body', 'body', 'required');
		$this->form_validation->set_rules('parent', 'parent', 'required');
		if ($this->form_validation->run() == FALSE) {
			redirect ('/comment/add/'.$this->input->post('parent'));
		} else {
			$author = $this->input->post('author');
			$body = $this->input->post('body');
			$parent = $this->input->post('parent');
			$tags = preg_split ("/, */", $this->input->post ('tags'));
			$this->MComment->add_comment($author, $body, $parent);
			redirect ('');
		}
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/link.php */

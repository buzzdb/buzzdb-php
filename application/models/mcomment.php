<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once ('db.php');
require_once(APPPATH.'third_party/sag/src/Sag.php');

class MComment extends CI_Model {
    protected $sag;

    function __construct() {
        parent::__construct();
		$this->sag = create_sag ();
    }
	public function add_comment ($author, $body, $parent_id) {
		$parent = $this->get_by_id($parent_id);
		if ($parent->path === NULL) {
			$path = array($parent_id);
		} else {
			$path = array_merge($parent->path, array($parent_id));
		}
		$object = array(
						'author' => $author,
						'body' => $body,
						'path' => $path,
						'type' => COMMENT_TYPE
						);
		$this->sag->post($object);
	}
	public function get_by_id($id) {
		return $this->sag->get($id)->body;
	}
	public function from_article ($id) {
		return get_view($this->sag, 'comments', $id);
	}
		
}
	   


<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once ('db.php');
require_once(APPPATH.'third_party/sag/src/Sag.php');
function get_host_from_url($url)
{
  return preg_replace ('/^http:\/\/([^\/]+).*/U', '\1', $url);
}
class MLink extends CI_Model {
    protected $sag;

    function __construct() {
        parent::__construct();
		$this->sag = create_sag ();
    }

    function get_last_entries() {
        return get_view($this->sag, 'alllinks');
    }
	public function add_link($url, $title, $tags) {
		$object = array(
					  'url' => $url,
					  'title' => $title,
					  'type' => LINK_TYPE,
					  'host' => get_host_from_url ($url),
					  'author' => 'John Doe',
					  'tags' => $tags);
		$this->sag->post($object);
	}
	public function get_by_id($id) {
		return $this->sag->get($id)->body;
	}
}

/* End of file mlink.php */
/* Location: ./application/model/mlink.php */

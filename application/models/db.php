<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH.'third_party/sag/src/Sag.php');

define(LINK_TYPE, 'link');
define(COMMENT_TYPE, 'comment');

define (DB_NAME, 'buzzdb');
define (DB_HOST, '127.0.0.1');
define (DB_PORT, 5984);

function create_sag()
{
	$r = new Sag('127.0.0.1', '5984');
	$r->setDatabase(DB_NAME);
	return $r;
}


function get_view ($sag, $view, $arg=null)
{
	if ($arg == null)
		return $sag->get('_design/'.DB_NAME.'/_view/'.$view)->body->rows;
	else {
		return $sag->get('_design/'.DB_NAME.'/_view/'.$view.'/?key='.json_encode($arg))->body->rows;
	}
}
	
<?php
function view_comment($comment)
{
	echo '<div class="comment_header">';
	echo 'Par ' . $comment->author . " — <a href=".site_url('comment/add/'.$comment->_id).">Répondre</a> </div>\n";
	echo '<div class="comment_body">'.$comment->body .'</div>';
}

function view_comments($comments)
{
	$path_len = 0;
	foreach ($comments as $comment) {
		echo "\n";
		for($k = 0; $k <= $path_len - count($comment->value->path); $k++)
			echo "</div>\n";
		echo '<div class=comment>'."\n";
		view_comment($comment->value);
		$path_len = count($comment->value->path);
	}
	for($k = 0; $k <= $path_len; $k++)
		echo "</div>\n";
}

?>

<?php
require(APPPATH.'views/header.php');
require(APPPATH.'views/comment.php');
?>
    <section id="articles">
        <article>
            <p>
                <a href="<?=$link->url?>"><?=$link->title?></a> <span class="host">(<?=$link->host?>)</span>
                <div class="tags">
                <?php foreach($link->tags as $t): ?>
                    <span><?=$t?></span>
                <?php endforeach ?>
            </p>
        </article>
    </section>
    <section id="comments">
		 <a href="<?=site_url('comment/add/'.$link->_id);?>">Commenter</a>
		 <?php view_comments($comments); ?>
    </section>

<?php require(APPPATH.'views/footer.php'); ?>

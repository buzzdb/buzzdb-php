<?php require(APPPATH.'views/header.php'); $this->load->helper('form'); ?>

    <section id="form_add_link">
        <?php echo form_open ('comment/add_commit'); ?>
        <table>
            <tr>
                <td><label>Pseudo : </label></td>
                <td><?php echo form_input ('author', ''); ?></td>
            </tr>
            <tr>
                <td><label>Commentaire : </label></td>
                <td><?php echo form_textarea ('body'); ?></td>
            </tr>
        </table>
    	 <?php echo form_hidden ('parent', $parent); ?>
        <?php echo form_submit('submit', 'Poster le commentaire'); ?>
        <?php echo form_close (); ?>
         
    </section>
<?php require(APPPATH.'views/footer.php'); ?>

<?php require(APPPATH.'views/header.php'); $this->load->helper('form'); ?>

    <section id="form_add_link">
        <?php echo form_open ('link/add_commit'); ?>
        <table>
            <tr>
                <td><label>Lien : </label></td>
                <td><?php echo form_input ('url', ''); ?></td>
            </tr>
            <tr>
                <td><label>Titre : </label></td>
                <td><?php echo form_input ('title', ''); ?></td>
            </tr>
            <tr>
                <td><label>Mots-clés (séparés par une virgule) : </label></td>
                <td><?php echo form_input ('tags', ''); ?></tr>
            </tr>
        </table>
        <?php echo form_submit('submit', 'Poster le lien'); ?>
        <?php echo form_close (); ?>
         
    </section>
<?php require(APPPATH.'views/footer.php'); ?>

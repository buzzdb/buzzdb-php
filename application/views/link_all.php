<?php require(APPPATH.'views/header.php'); ?>

    <section id="articles">
        <?php foreach($links as $l): ?>
        <article>
            <p>
                <a href="<?=$l->value->url?>"><?=$l->value->title?></a> <span class="host">(<?=$l->value->host?>)</span> —
	   <a href="<?=base_url()."index.php/link/view/".$l->value->_id?>">Commentaires</a>
                <div class="tags">
                <?php foreach($l->value->tags as $t): ?>
                    <span><?=$t?></span>
                <?php endforeach ?>
            </p>
        </article>
        <?php endforeach ?>
    </section>

<?php require(APPPATH.'views/footer.php'); ?>
